<?php

Route::get('/', ['uses' => 'AdminController@index', 'as' => 'admin.index']);
Route::get('/rss', ['uses' => 'AdminController@rss', 'as' => 'admin.rss']);
Route::post('/rss/{id}/restore', ['uses' => 'RssController@restore', 'as' => 'rss.restore']);
Route::get('/rss/{id}/show', ['uses' => 'RssController@show', 'as' => 'rss.show']);
Route::resource('rss', 'RssController', ['except' => ['show', 'index']]);