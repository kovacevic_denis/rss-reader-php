<?php

use App\Rss;
use Illuminate\Database\Seeder;

class RssSeeder extends Seeder
{

    protected $FEEDS = [
        [
            'user_id' => 1,
            'name' => 'www.php.net',
            'url' => 'http://www.php.net/news.rss ',
        ],
        [
            'user_id' => 1,
            'name' => 'slashdot.org',
            'url' => 'http://slashdot.org/rss/slashdot.rss',
        ],
        [
            'user_id' => 1,
            'name' => 'newsrss.bbc.co.uk',
            'url' => 'http://newsrss.bbc.co.uk/rss/newsonline_uk_edition/front_page/rss.xml',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->FEEDS as $feed) {
            $rss = new Rss($feed);
            $rss->save();
        }
    }
}
