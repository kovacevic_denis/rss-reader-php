# Time

| Feature | Description |Time |
| ------ | ------ | ------ |
| Analyze Aproach | I have decide to go with Laravel 5.5 | 1h
| Analyze Feed Parser | I have decide to use php library http://simplepie.org/ | 1h
| Feed Parser Implementation | \App\Packages\Feed\FeedFactory | 3h
| Controllers, models, repositories implementation | Set up architecture and integration  | 3h
| UI Implementation | Implement UI components using bootstrap | 3h
| QA | Testing | 1h