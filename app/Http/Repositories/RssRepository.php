<?php

namespace App\Http\Repositories;

use App\Rss;
use Illuminate\Http\Request;

/**
 * Class RssRepository
 * @package App\Http\Repositories
 */
class RssRepository
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        $name = $request->get('name');
        $url = $request->get('url');

        $rss = auth()->user()->rss()->create([
            'name' => $name,
            'url' => $url
        ]);

        return $rss;
    }

    /**
     * @param int $userId
     * @param int $page
     * @return Rss[]
     */
    public function findAllByUserId(int $userId, int $page = 20)
    {
        return Rss::where('user_id', $userId)->orderBy('created_at', 'desc')->paginate($page);
    }

    /**
     * @param Request $request
     * @param Rss $rss
     * @return Rss
     */
    public function update(Request $request, Rss $rss)
    {
        $name = $request->get('name');
        $url = $request->get('url');

        $rss->update([
            'name' => $name,
            'url' => $url
        ]);

        return $rss;
    }
}