<?php

namespace App\Http\Repositories;

use App\Rss;
use Illuminate\Http\Request;

/**
 * Class RssChannelRepository
 * @package App\Http\Repositories
 */
class RssChannelRepository
{

    /**
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        $name = $request->get('name');
        $url = $request->get('url');

        $rss = auth()->user()->rss()->create([
            'name' => $name,
            'url' => $url
        ]);

        return $rss;
    }

    /**
     * @param int $userId
     * @param int $page
     * @return Rss[]
     */
    public function myRssPaged(int $userId, int $page = 20)
    {
        return Rss::where('user_id', $userId)->orderBy('created_at', 'desc')->paginate($page);
    }
}