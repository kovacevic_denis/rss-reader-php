<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Repositories\RssRepository;
use App\Rss;
use App\Services\RssService;
use Illuminate\Http\Request;

/**
 * Class RssController
 * @package App\Http\Controllers\Admin
 */
class RssController extends Controller
{
    /** @var RssRepository */
    protected $rssRepository;

    /** @var RssService */
    protected $rssService;

    /**
     * RssController constructor.
     * @param RssRepository $rssRepository
     * @param RssService $rssService
     */
    public function __construct(RssRepository $rssRepository, RssService $rssService)
    {
        $this->rssRepository = $rssRepository;
        $this->rssService = $rssService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.rss.create');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validateRssForm($request);

        if ($this->rssRepository->create($request)) {
            return redirect('admin/rss')->with('success', 'Rss ' . $request['name'] . 'Created successfully');
        } else {
            return redirect('admin/rss')->withErrors('Rss ' . $request['name'] . 'Create failed');
        }
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $rss = Rss::findOrFail($id);
        $redirect = route('admin.rss');

        if (request()->has('redirect')) {
            $redirect = request()->input('redirect');
        }

        if ($rss->trashed()) {
            $result = $rss->forceDelete();
        } else {
            $result = $rss->delete();
        }

        if ($result) {
            return redirect($redirect)->with('success', 'Deleted successful');
        } else {
            return redirect($redirect)->withErrors('Delete Failed');
        }
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        $rss = Rss::findOrFail($id);

        if ($rss->trashed()) {
            $rss->restore();
            return redirect()->route('admin.rss')->with('success', 'Recovery successful');
        }
        return redirect()->route('admin.rss')->withErrors('Recovery failed');
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        /** @var Rss $rss */
        $rss = Rss::find($id);

        if ($rss->user_id != $this->getAuthUser()->id) {
            return redirect()->route('admin.rss')->withErrors('Invalid Permission');
        }

        return view('admin.rss.edit', compact('rss'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validateRssForm($request);

        /** @var Rss $rss */
        $rss = Rss::find($id);

        if ($rss->user_id != $this->getAuthUser()->id) {
            return redirect()->route('admin.rss')->withErrors('Invalid Permission');
        }


        if ($this->rssRepository->update($request, $rss)) {
            return redirect()->route('admin.rss')->with('success', 'Updated successful');
        } else {
            return redirect()->route('admin.rss')->with('success', 'Updating failed');
        }
    }

    /**
     * @param int $rssId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $rssId)
    {
        /** @var Rss $rss */
        $rss = Rss::find($rssId);

        $channel = $this->rssService->getChannel($rss->url);
        $rsses = $this->rssRepository->findAllByUserId($this->getAuthUser()->id);

        return view('admin.rss.show', compact('channel', 'rsses'));
    }

    /**
     * @param Request $request
     */
    private function validateRssForm(Request $request)
    {
        $v = [
            'name' => 'required',
            'url' => 'required',
        ];

        $this->validate($request, $v);
    }
}
