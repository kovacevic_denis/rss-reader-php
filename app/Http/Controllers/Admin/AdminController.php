<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Repositories\RssRepository;

/**
 * Class AdminController
 * @package App\Http\Controllers\Admin
 */
class AdminController extends Controller
{
    protected $rssRepository;

    /**
     * AdminController constructor.
     * @param RssRepository $rssRepository
     */
    public function __construct(RssRepository $rssRepository)
    {
        $this->rssRepository = $rssRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function rss()
    {
        $rsses = $this->rssRepository->findAllByUserId($this->getAuthUser()->id);

        return view('admin.rss', compact('rsses'));
    }
}
