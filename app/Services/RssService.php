<?php

namespace App\Services;

use App\Dto\Rss\Channel;
use App\Packages\Feed\AbstractFeed;
use App\Packages\Feed\FeedFactory;
use SimplePie;

/**
 * Class RssService
 * @package App\Services
 */
class RssService
{
    /** @var FeedFactory */
    protected $feedsFactory;

    /** @var Channel */
    protected $channel;

    /**
     * RssService constructor.
     * @param AbstractFeed $feedsFactory
     * @param Channel $channel
     */
    public function __construct(AbstractFeed $feedsFactory, Channel $channel)
    {
        $this->feedsFactory = $feedsFactory;
        $this->channel = $channel;
    }

    /**
     * @param $rssUrl
     * @return Channel
     */
    public function getChannel($rssUrl): Channel
    {
        /** @var SimplePie $feed */
        $feed = $this->feedsFactory->make($rssUrl);

        $channel = [
            'title' => $feed->get_title(),
            'description' => $feed->get_description(),
            'link' => $feed->get_link(),
            'image' => $this->getImage($feed)
        ];

        $items = [];

        /** @var \SimplePie_Item $item */
        foreach ($feed->get_items() as $item) {
            $items[] = [
                'title' => $item->get_title(),
                'description' => $item->get_description(),
                'link' => $item->get_link(),
                'thumbnail' => $item->get_thumbnail(),
                'pubDate' => $item->get_date(),
            ];
        }

        $channel['items'] = $items;

        return $this->channel->init($channel);
    }

    /**
     * @param SimplePie $feed
     * @return array|null
     */
    protected function getImage(SimplePie $feed)
    {
        $image = null;

        if (
            $feed->get_image_url() ||
            $feed->get_image_link() ||
            $feed->get_image_title()
        ) {
            $image = [
                'url' => $feed->get_image_url(),
                'link' => $feed->get_image_link(),
                'title' => $feed->get_image_title(),
            ];
        }

        return $image;
    }
}