<?php
namespace App\Parents\Exceptions;

use Exception;

/**
 * Class AbstractException
 * @package App\Parents\Exceptions
 */
abstract class AbstractException extends Exception
{

}