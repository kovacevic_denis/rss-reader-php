<?php

namespace App\Packages\Feed;

use SimplePie;

/**
 * Class AbstractFeed
 * @package App\Packages\Feed
 */
abstract class AbstractFeed
{
    /**
     * @param array $feed_url RSS URL
     * @param int $limit items returned per-feed with multifeeds
     * @param bool|false $force_feed
     *
     * @return SimplePie
     */
    abstract function make($feed_url = [], $limit = 0, $force_feed = false);
}
