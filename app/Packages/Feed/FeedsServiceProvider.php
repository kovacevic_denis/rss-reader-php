<?php

namespace App\Packages\Feed;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Application;

/**
 * Class FeedsServiceProvider
 * @package App\Packages\Feed
 */
class FeedsServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AbstractFeed::class, function (Application $app) {
            $config = config('feeds');

            if (!$config) {
                throw new \RunTimeException('Feeds configuration not found. Please run `php artisan vendor:publish`');
            }

            return $app->make(FeedFactory::class, [
                'config' => $config
            ]);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [AbstractFeed::class];
    }
}
