<?php

namespace App\Packages\Feed;

use SimplePie;

/**
 * Class FeedFactory
 * @package App\Packages\Feed
 */
class FeedFactory extends AbstractFeed
{

    protected $config;

    protected $simplePie;

    /**
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @param array $feed_url RSS URL
     * @param int $limit items returned per-feed with multifeeds
     * @param bool|false $force_feed
     *
     * @return SimplePie
     */
    public function make($feed_url = [], $limit = 0, $force_feed = false)
    {
        $this->simplePie = new SimplePie();
        $this->configure();
        $this->simplePie->set_feed_url($feed_url);
        $this->simplePie->set_item_limit($limit);
        if ($force_feed === true) {
            $this->simplePie->force_feed(true);
        }
        if (!$this->config['strip_html_tags.disabled'] && !empty($this->config['strip_html_tags.tags']) && is_array($this->config['strip_html_tags.tags'])) {
            $this->simplePie->strip_htmltags($this->config['strip_html_tags.tags']);
        } else {
            $this->simplePie->strip_htmltags(false);
        }
        if (!$this->config['strip_attribute.disabled'] && !empty($this->config['strip_attribute.tags']) && is_array($this->config['strip_attribute.tags'])) {
            $this->simplePie->strip_attributes($this->config['strip_attribute.tags']);
        } else {
            $this->simplePie->strip_attributes(false);
        }
        $this->simplePie->init();
        $this->simplePie->handle_content_type();

        return $this->simplePie;
    }

    protected function configure()
    {
        if ($this->config['cache.disabled']) {
            $this->simplePie->enable_cache(false);
        } else {
            $this->simplePie->set_cache_location($this->config['cache.location']);
            $this->simplePie->set_cache_duration($this->config['cache.life']);
        }
        if ($this->config['ssl_check.disabled']) {
            $this->simplePie->set_curl_options([
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false
            ]);
        }
    }
}
