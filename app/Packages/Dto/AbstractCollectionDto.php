<?php

namespace App\Packages\Dto;

use Illuminate\Support\Collection;

/**
 * Class AbstractCollectionDto
 * @package App\Packages\Dto
 */
abstract class AbstractCollectionDto extends Collection
{
    /**
     * @param $object
     * @return bool
     */
    abstract protected function isValidObject($object);

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        if (!$this->isValidObject($value)) {
            throw new \InvalidArgumentException('Invalid type for collection "' . get_class($this) . '"');
        }

        parent::offsetSet($offset, $value);
    }
}