<?php

namespace App\Packages\Dto\Providers;

use Illuminate\Support\ServiceProvider;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;

/**
 * Class DocExtractorProvider
 * @package App\Packages\Dto\Providers
 */
class DocExtractorProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    public function register()
    {
        $this->app->singleton(PhpDocExtractor::class, function ($app) {
            return new PhpDocExtractor();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            PhpDocExtractor::class
        ];
    }
}