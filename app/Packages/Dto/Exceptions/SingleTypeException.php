<?php

namespace App\Packages\Dto\Exceptions;

use App\Parents\Exceptions\AbstractException;

/**
 * Class SingleTypeException
 * @package App\Packages\Dto\Exceptions
 */
class SingleTypeException extends AbstractException
{
    public $message = 'Only Single Type is Allowed';
}
