<?php

namespace App\Packages\Dto;

use App\Packages\Dto\Exceptions\SingleTypeException;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Facades\App;
use JsonSerializable;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;

/**
 * Class AbstractDto
 * @package App\Packages\Dto
 */
abstract class AbstractDto implements JsonSerializable, Jsonable, Arrayable
{
    protected $hidden = [];

    /**
     * @var PropertyInfoExtractor
     * http://symfony.com/doc/master/components/property_info.html#phpdocextractor
     */
    protected $docExtractor;

    /**
     * @var array
     */
    private $attributesSet = [];

    protected $camelCase = true;

    /**
     * AbstractDto constructor.
     * @param array $attributes
     * @throws SingleTypeException
     */
    public function __construct($attributes = [])
    {
        $this->docExtractor = App::make(PhpDocExtractor::class);
        $this->init($attributes);
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function set(string $name, $value)
    {
        $this->removeAttributeSet($name);
        $this->$name = $value;
        $this->addAttributeSet($name);
    }

    /**
     * @param string $attribute
     */
    public function addAttributeSet(string $attribute)
    {
        $this->attributesSet[] = $attribute;
    }

    /**
     * @param string $attribute
     * @return bool
     */
    public function hasAttributeSet(string $attribute)
    {
        return in_array($attribute, $this->attributesSet);
    }

    /**
     * return an array of this object's properties
     * @return array
     */
    public function toArray()
    {
        $transformed = [];
        if (!empty($this->attributesSet)) {

            foreach ($this->attributesSet as $attribute) {
                if (!in_array($attribute, $this->hidden)) {
                    $transformed[$attribute] = $this->$attribute;
                }
            }
        }

        return $transformed;
    }

    /**
     * TODO refactor this, for the DB we will always use snake case
     * return an array of this object's properties
     * @return array
     */
    public function toArraySnakeCase()
    {
        $transformed = [];

        if (!empty($this->attributesSet)) {
            foreach ($this->attributesSet as $attribute) {

                if (!in_array($attribute, $this->hidden)) {
                    if ($this->$attribute instanceof AbstractDto) {
                        $transformed[snake_case($attribute)] = $this->$attribute->toArraySnakeCase();
                    } else {
                        $transformed[snake_case($attribute)] = $this->$attribute;
                    }
                }
            }
        }

        return $transformed;
    }

    /**
     * Convert the model to its string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toJson();
    }

    /**
     * Convert the model instance to JSON.
     *
     * @param  int $options
     * @return string
     *
     */
    public function toJson($options = 0)
    {
        $json = json_encode($this->jsonSerialize(), $options);
        return $json;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * @param array $attributes
     * @return $this
     * @throws SingleTypeException
     */
    public function init(array $attributes)
    {
        if (is_object($attributes)) {
            $attributes = get_object_vars($attributes);
        }

        foreach ($attributes as $attribute => $value) {
            $attribute = $this->formatAttribute($attribute);

            if (property_exists(get_class($this), $attribute)) {
                $value = $this->getValueByType($attribute, $value);

                $this->$attribute = $value;
                $this->addAttributeSet($attribute);
            }
        }

        return $this;
    }

    /**
     * @param $attribute
     * @return string
     */
    protected function formatAttribute(string $attribute): string
    {
        return $this->camelCase ? camel_case($attribute) : snake_case($attribute);
    }

    /**
     * @param $attribute
     * @param $value
     * @return array|mixed
     * @throws SingleTypeException
     */
    protected function getValueByType($attribute, $value)
    {
        if (is_object($value)) {
            return $value;
        }

        $types = $this->docExtractor->getTypes(get_class($this), $attribute);

        if (count($types) > 1) {
            throw new SingleTypeException();
        }

        if ($className = $this->getClassName($types, $isCollection)) {
            if ($isCollection) {
                $values = [];
                foreach ($value as $singleValue) {
                    $values[] = $this->createInstance($singleValue, $className);
                }
                $value = $values;
            } else {
                $value = $this->createInstance($value, $className);
            }
        }

        return $value;
    }

    /**
     * @param $attribute
     */
    protected function removeAttributeSet($attribute)
    {
        if (($key = array_search($attribute, $this->attributesSet)) !== false) {
            unset($this->attributesSet[$key]);
        }
    }

    /**
     * @param $types
     * @param bool $isCollection
     * @return null|string
     */
    protected function getClassName($types, &$isCollection)
    {
        $className = null;
        $isCollection = false;

        if (isset($types[0])) {
            $className = $types[0]->getClassName();
            if (!$className && $collectionType = $types[0]->getCollectionValueType()) {
                $className = $collectionType->getClassName();
                $isCollection = true;
            }
        }

        return $className;
    }

    /**
     * @param $value
     * @param $className
     * @return null
     * @throws SingleTypeException
     */
    protected function createInstance($value, $className)
    {
        $dto = null;

        if (!is_null($value)) {
            $dto = new $className();

            if ($dto instanceof AbstractDto) {
                $dto->init($value);
            }
        }

        return $dto;
    }
}