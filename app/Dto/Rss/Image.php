<?php
namespace App\Dto\Rss;

use App\Packages\Dto\AbstractDto;

/**
 * Class Image
 * @package App\Dto\Rss
 */
class Image extends AbstractDto
{
    /** @var string */
    public $url;

    /** @var string */
    public $title;

    /** @var string */
    public $link;
}