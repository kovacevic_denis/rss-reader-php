<?php
namespace App\Dto\Rss;

use App\Packages\Dto\AbstractDto;

/**
 * Class Thumbnail
 * @package App\Dto\Rss
 */
class Thumbnail extends AbstractDto
{
    /** @var int */
    public $width;

    /** @var int */
    public $height;

    /** @var string */
    public $url;
}