<?php
namespace App\Dto\Rss;

use App\Packages\Dto\AbstractDto;

/**
 * Class Channel
 * @package App\Dto\Rss
 */
class Channel extends AbstractDto
{
    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var string */
    public $link;

    /** @var Image */
    public $image;

    /** @var Item[] */
    public $items;
}