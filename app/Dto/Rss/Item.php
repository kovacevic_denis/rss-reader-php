<?php
namespace App\Dto\Rss;

use App\Packages\Dto\AbstractDto;

/**
 * Class Item
 * @package App\Dto\Rss
 */
class Item extends AbstractDto
{
    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var string */
    public $link;

    /** @var Thumbnail */
    public $thumbnail;

    /** @var string */
    public $pubDate;
}