
require('./boot');

(function ($) {
    let Xblog = {
        init: function () {
            this.bootUp();
            $('[data-toggle="tooltip"]').tooltip();
        },
        bootUp: function () {
            console.log('bootUp');
            initTables();
            autoSize();
            initDeleteTarget();
        },
    };

    function initDeleteTarget() {
        $('.swal-dialog-target').append(function () {
            return "\n" +
                "<form action='" + $(this).attr('data-url') + "' method='post' style='display:none'>\n" +
                "   <input type='hidden' name='_method' value='" + ($(this).data('method') ? $(this).data('method') : 'delete') + "'>\n" +
                "   <input type='hidden' name='_token' value='" + RssConfig.csrfToken + "'>\n" +
                "</form>\n"
        }).click(function () {
            let deleteForm = $(this).find("form");
            let method = ($(this).data('method') ? $(this).data('method') : 'delete');
            let url = $(this).attr('data-url');
            let data = $(this).data('request-data') ? $(this).data('request-data') : '';
            let title = $(this).data('dialog-title') ? $(this).data('dialog-title') : 'Delete';
            let message = $(this).data('dialog-msg');
            let type = $(this).data('dialog-type') ? $(this).data('dialog-type') : 'warning';
            let cancel_text = $(this).data('dialog-cancel-text') ? $(this).data('dialog-cancel-text') : 'Cancel';
            let confirm_text = $(this).data('dialog-confirm-text') ? $(this).data('dialog-confirm-text') : 'Confirm';
            let enable_html = $(this).data('dialog-enable-html') == '1';
            let enable_ajax = $(this).data('enable-ajax') == '1';
            console.log(data);
            if (enable_ajax) {
                swal({
                        title: title,
                        text: message,
                        type: type,
                        html: enable_html,
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        cancelButtonText: cancel_text,
                        confirmButtonText: confirm_text,
                        showLoaderOnConfirm: true,
                        closeOnConfirm: true
                    },
                    function () {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': RssConfig.csrfToken
                            },
                            url: url,
                            type: method,
                            data: data,
                            success: function (res) {
                                if (res.code == 200) {
                                    swal({
                                        title: 'Succeed',
                                        text: res.msg,
                                        type: "success",
                                        timer: 1000,
                                        confirmButtonText: "OK"
                                    });
                                } else {
                                    swal({
                                        title: 'Failed',
                                        text: "Failed",
                                        type: "error",
                                        timer: 1000,
                                        confirmButtonText: "OK"
                                    });
                                }
                            },
                            error: function (res) {
                                swal({
                                    title: 'Failed',
                                    text: "Failed",
                                    type: "error",
                                    timer: 1000,
                                    confirmButtonText: "OK"
                                });
                            }
                        })
                    });
            } else {
                swal({
                        title: title,
                        text: message,
                        type: type,
                        html: enable_html,
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        cancelButtonText: cancel_text,
                        confirmButtonText: confirm_text,
                        closeOnConfirm: true
                    },
                    function () {
                        deleteForm.submit();
                    });
            }
        });
    }

    function initTables() {
        $('.post-detail-content table').addClass('table table-striped table-responsive');
    }

    function autoSize() {
        autosize($('.autosize-target'));
    }

    window.Xblog = Xblog;
})(jQuery);
$(document).ready(function () {
    Xblog.init();
});
