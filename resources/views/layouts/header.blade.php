<header class="main-header bg-placeholder">
    <div class="container-fluid" style="margin-top: -15px">
        <nav class="navbar navbar-dark navbar-expand-lg">
            <a href="/" id="blog-navbar-brand" class="navbar-brand">Rss Application</a>
            <button type="button" class="navbar-toggler" data-toggle="collapse"
                    data-target="#blog-navbar-collapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="blog-navbar-collapse">
                <ul class="nav navbar-nav ml-auto justify-content-end">
                    @if(Auth::check())
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink"
                               data-toggle="dropdown">
                                <?php
                                $user = auth()->user();
                                ?>
                                {{ $user->name }}
                                <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('admin.rss') }}">Dashboard</a>
                                <a class="dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    @else
                        <li class="nav-item"><a class="nav-link" href="{{ url('login') }}">Login</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('register') }}">Register</a></li>
                    @endif

                </ul>
            </div>
        </nav>
    </div>
    <div class="container-fluid">
        <div class="description">@yield('channel')</div>
    </div>
</header>