<!DOCTYPE html>
<html lang="en" xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta property="og:type" content="website">
    <meta name="theme-color" content="#52768e">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('css')
    <script>
        window.RssConfig = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body id="body">
@include('layouts.header')
<div id="content-wrap">
    <div class="container">
        @includeWhen(!isset($include_msg) || $include_msg, 'partials.msg')
    </div>
    @yield('content')
</div>
@include('layouts.footer')
<script src="{{ mix('js/app.js') }}"></script>
@yield('script')
</body>
</html>
