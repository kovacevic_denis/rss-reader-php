<footer class="footer" id="footer">
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span>Copyright © Kovacevic Denis</span> |
                    <span>Powered by Laravel</span>
                </div>
            </div>
        </div>
    </div>
</footer>