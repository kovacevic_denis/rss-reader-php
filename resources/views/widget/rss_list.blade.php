<div class="card mb-3">
    <div class="card-header bg-white"><i class="fa fa-folder fa-fw"></i><a class="text-dark" href="{{ route('admin.rss') }}">All Rss</a></div>
    <div class="card-body">
        <div class="list-group hot-posts">
            @forelse($rsses as $rss)
                <a title="{{ $rss->name }}" href="{{ route('rss.show',$rss->id) }}"
                   class="list-group-item list-group-item-action">
                    {{ $rss->name }}
                </a>
            @empty
                <p class="meta-item center-block">No Rss Feeds.</p>
            @endforelse
        </div>
    </div>
</div>