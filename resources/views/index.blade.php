@extends('layouts.plain')
@section('content')
    <div class="home-box">
        <h3 title="Rss Application" aria-hidden="true" style="margin: 0">
            Rss Application
        </h3>
        <p class="links">
            <font aria-hidden="true">»</font>
            <a href="{{ route('login') }}" aria-label="Please login">Please login</a>
        </p>
    </div>
@endsection