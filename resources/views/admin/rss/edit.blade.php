@extends('admin.layouts.app')
@section('title', 'Edit ' . $rss->title)
@section('content')
    <div id="data" data-id="{{ $rss->id }}">
        <div class="card-body edit-form">
            <form role="form" class="form-horizontal" action="{{ route('rss.update', $rss->id) }}"
                  method="post">
                @include('admin.rss.form-content')
                <input type="hidden" name="_method" value="put">
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection