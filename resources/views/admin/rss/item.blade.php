<article class="post card">
    <!-- post header -->
    <div class="post-header">
        <h1 class="post-title">
            <a title="{{ $item->title }}" href="{{ $item->link }}">
                {{ $item->title }}
            </a>
        </h1>
        <div class="post-meta">
                           <span class="post-time">
                           <i class="fa fa-calendar-o"></i>
                           <time datetime="2016-08-05T00:10:14+08:00">
                           {{ $item->pubDate }}
                           </time>
                           </span>
            @if($item->thumbnail)
                <img src={{ $item->thumbnail->url }} class="img-fluid">
            @endif

        </div>
    </div>
    {{--post content--}}
    <div class="post-description">
        {!! $item->description !!}
    </div>
</article>