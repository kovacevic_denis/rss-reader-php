<div class="form-group">
    <label for="name" class="form-control-label">Name*</label>
    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
           value="{{ isset($rss) ? $rss->name : old('name') }}"
           autofocus>
    @if ($errors->has('name'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('name') }}</strong>
        </div>
    @endif
</div>
<div class="form-group">
    <label for="url" class="form-control-label">Url*</label>
    <input id="url" type="text" class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}" name="url"
           value="{{ isset($rss) ? $rss->url : old('url') }}"
           autofocus>
    @if ($errors->has('url'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('url') }}</strong>
        </div>
    @endif
</div>
{{ csrf_field() }}