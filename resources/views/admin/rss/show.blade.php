@extends('layouts.app')
@section('title','Rss feed')
@section('channel')
    {{ $channel->title }}
@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                @if(!$channel->items)
                    <h3 class="meta-item center-block">NO POSTS.</h3>
                @else
                    @each('admin.rss.item',$channel->items,'item')
                @endif
            </div>
            <div class="col-md-4">
                <div class="slide">
                    @include('layouts.widgets')
                </div>
            </div>
        </div>
    </div>
@endsection
