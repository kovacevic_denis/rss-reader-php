@extends('admin.layouts.app')
@section('title', 'Rss create')
@section('content')
    <div class="edit-form">
        <form class="form-horizontal" action="{{ route('rss.store') }}" method="post">
            @include('admin.rss.form-content')
            <button type="submit" class="btn btn-primary">
                Create
            </button>
        </form>
    </div>
@endsection