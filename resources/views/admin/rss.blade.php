@extends('admin.layouts.app')
@section('title','Rss')
@section('content')
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>Name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rsses as $rss)
            <tr>
                <td title="{{ $rss->name }}">{{ str_limit($rss->name,64) }}</td>
                <td>
                    <div>
                        <a {{ $rss->trashed()?'disabled':'' }} href="{{ $rss->trashed()?'javascript:void(0)':route('rss.edit',$rss->id) }}"
                           data-toggle="tooltip" data-placement="top" title="Edit"
                           class="btn btn-info">
                            <i class="fa fa-pencil fa-fw"></i>
                        </a>
                        @if($rss->trashed())
                            <form style="display: inline" method="post" action="{{ route('rss.restore',$rss->id) }}">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                        data-placement="top" title="Restore">
                                    <i class="fa fa-repeat fa-fw"></i>
                                </button>
                            </form>
                        @else
                            <a href="{{ route('rss.show',$rss->id) }}" data-toggle="tooltip"
                               data-placement="top" title="Show"
                               class="btn btn-success">
                                <i class="fa fa-eye fa-fw"></i>
                            </a>
                        @endif
                        <button class="btn btn-danger swal-dialog-target"
                                data-toggle="tooltip"
                                data-title="{{ $rss->name }}"
                                data-dialog-msg="Do you want to delete <label>{{ $rss->name }}</label>？"
                                title="Delete"
                                data-dialog-enable-html="1"
                                data-url="{{ route('rss.destroy',$rss->id) }}"
                                data-dialog-confirm-text="{{ $rss->trashed()?'Delete (this will be permanently deleted)':'Delete' }}">
                            <i class="fa fa-trash-o  fa-fw"></i>
                        </button>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

