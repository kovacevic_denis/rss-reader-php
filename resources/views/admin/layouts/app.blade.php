<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="theme-color" content="#52768e">
    <title>@yield('title')</title>
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">
    @yield('css')
    <script>
        window.RssConfig = <?php echo json_encode([
            'csrfToken' => csrf_token()
        ]);?>
    </script>
</head>
<body>
<div class="main">
    <div class="sidebar-wrapper bg-placeholder" id="sidebar-wrapper">
        <div class="p-3">
            <div class="collapse sidebar" id="sidebar">
                <?php
                $menus = [
                    [
                        'name' => 'Rss',
                        'icon' => 'list-alt',
                        'route' => 'admin.rss'
                    ],
                    [
                        'name' => 'Add Rss',
                        'icon' => 'plus',
                        'route' => 'rss.create'
                    ]
                ]
                ?>
                <div class="nav-wrapper">
                    <nav class="nav flex-column nav-pills font-weight-bold">
                        @foreach( $menus as $menu)
                            @if(isset($menu['is_parent']) && $menu['is_parent'])
                                <?php
                                foreach ($menu['children'] as $children_menu) {
                                    if (route($children_menu['route']) == request()->url()) {
                                        $show = true;
                                        break;
                                    } else
                                        $show = false;
                                }
                                ?>
                                <a class="nav-link{{ $show ? ' active':' collapsed' }}" role="tab" data-toggle="collapse" href="#{{ $menu['name'] }}" aria-expanded="false">
                                    <i class="fa fa-{{ $menu['icon'] }} fa-fw mr-3"></i>
                                    {{ $menu['name'] }}
                                </a>
                                <div class="collapse {{ $show ? ' show':'' }}" id="{{ $menu['name'] }}">
                                    <div class="nav-wrapper">
                                        <nav class="nav nav-pills flex-column">
                                            @foreach( $menu['children'] as $children_menu)
                                                <?php $link = route($children_menu['route']);?>
                                                <a class="ml-3 my-1 nav-link {{ $link == request()->url() ? ' active':'' }}" role="tab" href="{{ $link }}">
                                                    <i class="fa fa-{{ $children_menu['icon'] }} fa-fw mr-3"></i>
                                                    {{ $children_menu['name'] }}
                                                </a>
                                            @endforeach
                                        </nav>
                                    </div>
                                </div>
                            @else
                                <?php $link = route($menu['route']);?>
                                <a class="nav-link {{ $link == request()->url() ? ' active':'' }}" role="tab" href="{{ $link }}">
                                    <i class="fa fa-{{ $menu['icon'] }} fa-fw mr-3"></i>
                                    {{ $menu['name'] }}
                                </a>
                            @endif
                        @endforeach
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <nav class="navbar navbar-expand-lg navbar-light bg-light pt-1 pb-1" style="font-size: 85%">
            <?php $user = auth()->user();?>
            <a class="navbar-brand" href="{{ route('admin.index') }}">
                {{ $user->name }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon" style="width: 1.25rem;height: 1.25rem;"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <ul class="nav navbar-nav ml-auto justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                    </li>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </ul>
            </div>
        </nav>
        <div class="pt-3 pr-3 pl-3">
            <div class="content-header">
                <div class="content-header-title">
                    <h2 class="content-header-title-det mt-0">@yield('title')</h2>
                </div>
                <div class="content-header-action">
                    @yield('action')
                </div>
            </div>
            <hr class="divider mt-3">
        </div>
        <div class="p-3">
            @include('partials.msg')
            @yield('content')
        </div>
    </div>
</div>

<script src="{{ mix('js/app.js') }}"></script>
@yield('script')
</body>
</html>
