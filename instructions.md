# Requirements
 - GIT
 - PHP >= 7.1
 - Composer
 - Node
 - Web Server (Nginx is recommended)
 - Database Engine mysql
 - Cache Engine Redis
 
# Installation using Laravel Homestead
### Requirements
 - check https://laravel.com/docs/5.5/homestead
### Installation
- ```git clone https://bitbucket.org/kovacevic_denis/rss-reader-php```
- ```cd rss-reader-php/```
- ```cp .env.example  .env```
- ```composer install```
- ```vendor/bin/homestead make```
- ```vagrant up```
- ```vagrant ssh```
- ```cd code/```
- ```composer dump-autoload```
- ```php artisan migrate```
- ```php artisan db:seed```
- ```sudo npm install```
- ```npm run development```
- Edit your hosts file on host machine:
```
192.168.10.10 rss.test
```
- User to Login in
```
email: admin@admin.com
password: 111111
```
- Enjoy http://rss.test